'use strict';

module.exports.config = {

    allScriptsTimeout: 11000,

    specs: [
        'test/e2e/*.js'
    ],

    capabilities: {
        'browserName': 'firefox'
    },

    baseUrl: 'http://localhost:8000/',

    //use default one
    seleniumServerJar: null,

    onCleanUp: function(){
        throw new Error('Dummy error for shutting down servers');
    },

    framework: 'jasmine',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    }
};