'use strict';

define([
	'angular'
], function(angular) {
	angular.module('myApp.directives', []);
});
