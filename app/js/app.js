'use strict';

define([
	'angular',
	'filters',
	'services',
	'directives',
	'controllers',
	'angularUIRouter',
    'ngTable'
], function (angular, filters, services, directives, controllers) {
    return angular.module('myApp', [
        'ui.router',
        'ngTable',
        'myApp.filters',
        'myApp.services',
        'myApp.directives',
        'myApp.controllers'
    ]);
});
