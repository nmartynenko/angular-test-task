'use strict';

define([
  'angular', 
  'angularResource'
], function (angular) {
	angular.module('myApp.services', ['ngResource'])
		.factory('People', function($resource){
            return $resource('app/api/v1/test-data.json', {}, {
                'query': {method: 'GET', isArray: false }
            });
  		});
});
