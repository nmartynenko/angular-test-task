# About

From customers requirements:

    HI. I have a simple test (this is your interview) for you to complete  before we start. I've attached a json file that contains an array of  people with name, age, and sex. Create an angular js driven html page  that reads this file and displays the contents in a organized table.  Please use Bootstrap http://getbootstrap.com for the css.  The table should be filterable and sortable on first name, last name, age, and sex.
    Also provide jasmine unit tests using karma with grunt as the task  runner.  Package everything up in a zip file and send it back to me.  I  want to be able to unzip the web app into an apache web server and run  grunt on the karma tests with code coverage.

Additional requirements:

    Yes I've checked the code.  Running grunt fails.  Also I wanted to be able to filter and sort the rendered table.  Can you resubmit your code?  You can use gulp if you like.
    
    These are the requirements I'm looking for:
    
        It must produce unit test report
        It must produce code coverage report
        I can run a web server on the build
        The rendered table must be filterable
        The rendered table must be sortable on all fields 
    
    
    Email me a zip or tgz file of the code.  I should be able to unpack the file, run the build (grunt or gulp), and deploy into a web server (or have grunt start one).

## Installation

Install [bower](http://bower.io/):

    npm install -g bower

Install [gulp](http://gulpjs.com/):

    npm install -g gulp

__Note__: you can install several components in one script, e.g. `npm install -g bower gulp`

Install components:

    bower install
    npm install
    
Install selenium server (for e2e tests):
    
    ./node_modules/.bin/webdriver-manager update
    
## Installation troubleshooting

Some node components need to be compiled with [node-gyp](https://github.com/TooTallNate/node-gyp). If you have errors in `npm-debug.log` somehow related with it, please check [installation pre-requisites](https://github.com/TooTallNate/node-gyp#installation).

Typically you don't need anything to do on *Nix systems, though it might be some sort of challenging in Windows. Fortunately official installation guide is pretty clear and accurate.
    
## Running

For applying Karma tests (single run) you need to run:

    gulp test
    
For integration (E2E) tests you need to run:

    gulp e2e
    
For development purposes you can start [Express.JS](http://expressjs.com/) server with live-reload capabilities and background Karma-server:
    
    gulp start
    
Go into browser [http://localhost:8000/](http://localhost:8000/)

Also you can review coverage tests here `http://localhost:8000/coverage/<<BROWSER_TYPE>>/js/index.html`, f.e. [http://localhost:8000/coverage/PhantomJS%201.9.7%20%28Linux%29/js/index.html](http://localhost:8000/coverage/PhantomJS%201.9.7%20%28Linux%29/js/index.html)

For creating release simply apply task:
    
    gulp release
    
Unzip archive to Apache 2 web folder
    
    unzip build/release.zip /path/to/apache/www
    
Go into browser [http://localhost/somepath](http://localhost/somepath)