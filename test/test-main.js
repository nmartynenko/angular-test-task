/* global window: false */

'use strict';

var allTestFiles = [];
var TEST_REGEXP = /spec\.js$/;

var pathToModule = function(path) {
    //go to 2 levels higher, e.g. from '/base/app/js' to '/base'
    return '../../' + path.replace(/^\/base\//, '').replace(/\.js$/, '');
};

Object.keys(window.__karma__.files).forEach(function(file) {
    if (TEST_REGEXP.test(file)) {
        // Normalize paths to RequireJS module names.
        allTestFiles.push(pathToModule(file));
    }
});

require.config({
    // Karma serves files from '/base'
    //use our sources root
    baseUrl: '/base/app/js',

    paths: {
        angular:         '/base/app/bower_components/angular/angular',
        angularMocks:    '/base/app/bower_components/angular-mocks/angular-mocks',
        angularResource: '/base/app/bower_components/angular-resource/angular-resource',
        angularUIRouter: '/base/app/bower_components/angular-ui-router/release/angular-ui-router',
        ngTable:         '/base/app/bower_components/ng-table/ng-table',
        bootstrap:       '/base/app/bower_components/bootstrap/dist/js/bootstrap',
        domReady :       '/base/app/bower_components/requirejs-domready/domReady',
        jquery:          '/base/app/bower_components/jquery/dist/jquery',
        text:            '/base/app/bower_components/requirejs-text/text'
    },

    shim: {
        'angular' : {
            deps : [
                'domReady',
                'jquery'
            ],
            exports : 'angular'
        },
        'angularResource': [ 'angular' ],
        'angularMocks': {
            deps: [ 'angular' ],
            exports :'angular.mock'
        },
        'angularUIRouter' : {
            deps : [
                'angular'
            ]
        },
        'ngTable' : {
            deps : [
                'angular'
            ]
        },
        jquery : {
            exports : '$'
        },
        bootstrap : {
            deps : [
                'jquery'
            ]
        }
    },

    priority: [
        'angular'
    ],

    // dynamically load all test files
    deps: allTestFiles,

    // we have to kickoff jasmine, as it is asynchronous
    callback: window.__karma__.start
});