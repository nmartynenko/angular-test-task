/* global describe: false,
 it: false,
 expect: false,
 jasmine: false,
 beforeEach: false,
 afterEach: false,
 inject: false */

'use strict';

define(['app', 'jquery', 'text!../../app/api/v1/test-data.json', 'angularMocks'], function(app, $, data){
    describe('Application', function() {

        it('should contain jQuery', function() {
            expect($).toBeDefined();
            expect($().jquery).toBeDefined();
        });

        it('should contain angular global object', function() {
            expect(angular).toBeDefined();
        });

        it('should contain angular.mock global object', function() {
            expect(angular.mock).toBeDefined();
        });

        it('should contain jasmine global object', function() {
            expect(jasmine).toBeDefined();
        });

        it('should contain test data', function() {
            expect(data).toBeDefined();
        });

        it('should be defined', function() {
            expect(app).toBeDefined();
            expect($.isPlainObject(app)).toBeTruthy();
        });

   });

    //if it is defined, then convert text data to JSON
    var jsonData = JSON.parse(data);

    describe('Interpolate filters', function() {
        var interpolateFilter,
            //version should be any
            version = new Date().getTime();

        //load module
        beforeEach(module('myApp'));

        beforeEach(
            module(function($provide) {
                $provide.value('version', version);
            })
        );

        beforeEach(
            inject(function($filter){
                interpolateFilter = $filter('interpolate');
            })
        );

        it('should contain interpolate filter', function(){
            expect(interpolateFilter).toBeDefined();
        });

        it('should replace %VERSION% with the value we provide', function(){
            var initialString = 'version is %VERSION%',
                resultString =  'version is ' + version;

            expect(interpolateFilter(initialString)).toEqual(resultString);
        });
    });

    describe('People controller', function() {
        var $scope,
            $httpBackend,
            createController;

        //load module
        beforeEach(module('myApp'));

        beforeEach(
            inject(function ($rootScope, $controller, _$httpBackend_) {
                $scope = $rootScope.$new();

                $httpBackend = _$httpBackend_;

                createController = function () {
                    return $controller('PeopleCtrl', {
                        '$scope': $scope
                    });
                };
            })
        );

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should contain People resource, and name matters',
            inject(function ($injector) {
                var correctResourceName = 'People',
                    wrongResourceNames = ['people', 'PEOPLE', 'aaaa'];

                expect($injector.get(correctResourceName)).toBeDefined();

                //iterate over bunch of wrong names
                $.each(wrongResourceNames, function (wrongResourceName) {
                    //anonymous function to wrap a throwable method
                    expect(function () {
                        $injector.get(wrongResourceName);
                    }).toThrow();
                });
            })
        );

        it('should process PeopleCtrl controller and put tableParams of type "ngTableParams" in $scope',
            inject(function (ngTableParams) {
                //call JSON data file
                $httpBackend
                    .expectGET('app/api/v1/test-data.json')
                    .respond(data);

                //create new controller
                var controller = createController();

                //it should be not null
                expect(controller).toBeDefined();

                //expect table params be not yet present in scope
                expect($scope.tableParams).toBeUndefined();

                //flush request
                $httpBackend.flush();

                //and then expect table params be present in scope
                expect($scope.tableParams).toBeDefined();

                //and then expect table params be type of ngTableParams
                expect($scope.tableParams).toEqual(jasmine.any(ngTableParams));
            })
        );

        it('should resolve data by default',
            inject(function ($q) {
                //call JSON data file
                $httpBackend
                    .expectGET('app/api/v1/test-data.json')
                    .respond(data);

                //pull controller
                createController();

                //flush request
                $httpBackend.flush();

                var spec = this,
                    tableParams = $scope.tableParams,
                    tableSettings = tableParams.settings(),
                    $defer = $q.defer();

                //make sure there sorting presents
                expect(tableParams.sorting()).toBeDefined();

                tableSettings.getData($defer, tableParams);

                $defer.promise.then(
                    function (users) {
                        //total number should be the same as responded data length
                        expect(tableSettings.total).toEqual(jsonData.people.length);

                        //number items on page should be greater then 2
                        expect(tableParams.count()).toBeGreaterThan(2);

                        //it should show first page with sorted values
                        expect(users.length).toEqual(tableParams.count());
                    }, function () {
                        spec.fail("there should be no error");
                    }
                );
            })
        );

        it('should resolve data in cases "filter()" and "sorting()" are null',
            inject(function ($q) {
                //call JSON data file
                $httpBackend
                    .expectGET('app/api/v1/test-data.json')
                    .respond(data);

                //pull controller
                createController();

                //flush request
                $httpBackend.flush();

                var spec = this,
                    tableParams = $scope.tableParams,
                    $defer = $q.defer();

                //set "filter()" and "sorting()" being null
                tableParams.filter(null);
                tableParams.sorting(null);

                //make sure they are null
                expect(tableParams.filter()).toBeNull();
                expect(tableParams.sorting()).toBeNull();

                tableParams.settings().getData($defer, tableParams);

                $defer.promise.then(
                    function (users) {
                        //it should show first page anyway
                        expect(users.length).toEqual(tableParams.count());
                    }, function () {
                        spec.fail("there should be no error");
                    }
                );
            })
        );

    });
});
