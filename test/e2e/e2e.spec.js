/* global describe: false,
 it: false,
 expect: false,
 jasmine: false,
 beforeEach: false,
 afterEach: false,
 by: false,
 browser: false,
 element: false */

'use strict';

describe('Application', function(){
    //go to index page
    browser.get('/');

    it('should load body', function(){
        element(by.css('body')).then(function(body){
            expect(body).toBeDefined();
        });
    });

    it('should render a only one table, and it\'s should contain "ng-table" class', function(){
        element.all(by.css('table')).then(function(items){
            //it should find 1 items
            expect(items.length).toEqual(1);

            //it should contain class 'ng-table'
            expect(items[0].getAttribute('class')).toContain('ng-table');
        });

    });

    it('should contain filter\'s input', function(){
        var filterInput = element.all(by.css('.input-filter'));
        expect(filterInput).toBeDefined();
    });

    it('filter\'s input should show empty table if you put random values', function(){
        var filterInput = element(by.className('input-filter'));

        filterInput.sendKeys('__It\'s unlikely is NOT filtering value__!!!11' + new Date().getTime()).then(function(){
            element.all(by.css("table tr")).then(function(trs){
                //there should be no more 2 TR tags (header and filter)
                expect(trs.length).toEqual(2);
            });
        });

    });
});
