/* jshint -W030 */
/* global __dirname: false */

'use strict';

/**
 * Components list
 */
var _ = require('lodash'),
    concat = require('gulp-concat'),
    del = require('del'),
    errorhandler = require('errorhandler'),
    express = require('express'),
    gulp = require('gulp'),
    htmlreplace = require('gulp-html-replace'),
    jshint = require('gulp-jshint'),
    jshintStylish = require('jshint-stylish'),
    karma = require('karma').server,
    KarmaConfig= require('karma/lib/config').Config,
    livereload = require('gulp-livereload'),
    morgan = require('morgan'),
    notify = require('gulp-notify'),
    protractor = require('gulp-protractor').protractor,
    sass = require('gulp-sass'),
    watch = require('gulp-watch'),
    zip = require('gulp-zip');

/**
 * Settings
**/
var livereloadport = 35729,
    serverport     = 8000,
    //references to servers
    livereloadServer,
    expressServer;

/**
 * Tasks implementation
**/
gulp.task('jshint', function(){
    gulp.src([
        //main files
        'app/js/**/*',
        //test files
        'test/**/*',
        //config files
        'config/**/*',
        //gulp file itself
        'Gulpfile.js'
    ])
        .pipe(jshint())
        //add fancy reporter
        .pipe(jshint.reporter(jshintStylish))
        //stop script when jshint check fail
        .pipe(jshint.reporter('fail'));
});


//create new karma config
var karmaConfig = new KarmaConfig();

//load common config
require('./config/karma.conf')(karmaConfig);

//override base path
karmaConfig.set({basePath: ''});

/**
 * Run test once and exit
 */
gulp.task('karma', function (done) {
    //set "singleRun: true" despite actual settings
    karma.start(_.assign({}, karmaConfig, {singleRun: true}), done);
});
/**
 * Watch for file changes and re-run tests on each change
 */
gulp.task('karma-tdd', function (done) {
    //set "singleRun: false" despite actual settings
    karma.start(_.assign({}, karmaConfig, {singleRun: false}), done);
});

gulp.task('protractor-e2e', function () {
    gulp.src(['./test/e2e/*.js'])
        .pipe(protractor({
            configFile: 'config/protractor.conf.js'
        }))
        .on('error', function(){
            console.log("Shutdown livereload server");
            livereloadServer && livereloadServer.close();
            console.log("Shutdown Express JS server");
            expressServer && expressServer.close();
        });
});

gulp.task('watch', function () {
    gulp.watch(['app/scss/**/*'], ['styles-lr']);
    gulp.watch(['app/js/**/*']).on('change', livereload.changed);
    gulp.watch(['index.html']).on('change', livereload.changed);
});

gulp.task('styles', function () {
    return gulp.src(['app/scss/**/*'])
        .pipe(sass())
        .pipe(concat('app.css'))
        .pipe(gulp.dest('app/css/'))
        .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('styles-lr', ['styles'], function () {
    return gulp
        .src(['app/css/app.css'])
        .pipe(livereload());
});

gulp.task('serve', function () {
    expressServer =
        express()
        .use(express.static(__dirname))
        .use(morgan('combined')) // logger
        .use(errorhandler()) // error handler
        .listen(serverport, function () {
            console.log('Started server on: ' + serverport);
            livereloadServer =
                livereload.listen(livereloadport, function () {
                    console.log('Started livereload server on: ' + livereloadport);
                });
        });
});

gulp.task('clean', function(done) {
    del(['build'], done);
});

gulp.task('replace-lr', ['clean'], function(){
    gulp
        .src([
            'index.html'
        ])
        //remove livereload script in production
        .pipe(htmlreplace({
            livereload : ''
        }))
        .pipe(gulp.dest('./build/'));
});

/**
 * Tasks definition
 **/
//test task
gulp.task('test', ['jshint', 'styles', 'karma']);
//tdd task
gulp.task('tdd', ['jshint', 'styles', 'karma-tdd']);
//e2e task
gulp.task('e2e', ['test', 'serve', 'protractor-e2e']);
//start server
gulp.task('start', ['watch', 'tdd', 'serve']);
//default task
gulp.task('default', ['test']);
//release task
gulp.task('release', ['test', 'replace-lr'], function(){
    gulp
        .src([
            './build/index.html',
            '**/app/**',
            '!coverage/**'
        ])
        .pipe(zip('release.zip'))
        .pipe(gulp.dest('./build/'));
});